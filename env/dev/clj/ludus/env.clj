(ns ludus.env
  (:require [selmer.parser :as parser]
            [clojure.tools.logging :as log]
            [ludus.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[ludus started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[ludus has shut down successfully]=-"))
   :middleware wrap-dev})
